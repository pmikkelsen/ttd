#ifndef GRID_H
#define GRID_H

double grid_pos_to_ndc(double);

int in_range(struct tower *, struct enemy *);

#endif  
