#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>

#include "rendering.h"
#include "gamelogic.h"
#include "input.h"

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		printf("Usage: %s /path/to/level/file\n", argv[0]);
		return -1;
	}
		
	GLFWwindow *window;

	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "TTD", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}

	glfwSetKeyCallback(window, key_callback);

	glfwMakeContextCurrent(window);
	glewInit();
	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	struct gamestate *gamestate = init_game(argv[1]);
	init_graphics(gamestate);

	double tick_delta = 1.0/60.0;
	double last_tick = 0.0;
	double time_delta;
	while (!glfwWindowShouldClose(window)) {
		time_delta = glfwGetTime() - last_tick;
		if (time_delta > tick_delta) {
			update_gamestate("levels/enemies", time_delta);
			last_tick = glfwGetTime();
		}

		render(gamestate);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	
	glfwTerminate();
	return 0;
}

