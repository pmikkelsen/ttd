#ifndef TTD_H
#define TTD_H

#include <sys/queue.h>

#define WINDOW_HEIGHT 800
#define WINDOW_WIDTH  1000
#define GRID_SIZE     20.0

TAILQ_HEAD(enemy_list, enemy);
TAILQ_HEAD(path_list, coord);
TAILQ_HEAD(tower_list, tower);
TAILQ_HEAD(bullet_list, bullet);

enum direction {
	SOUTH,
	EAST,
	NORTH,
	WEST,
	END
};

struct coord {
	double x;
	double y;
	enum direction dir_to_next;
	TAILQ_ENTRY(coord) coords;
};

struct enemy {
	double x;
	double y;
	double speed;
	double distance_traveled;
	double start_delay;
	double hp;
	int money_drop;
	struct path_list *path;
	struct coord *current;
	TAILQ_ENTRY(enemy) enemies;
};

struct tower {
	int x;
	int y;
	int damage;
	int range;
	double recharge_time;
	double time_to_ready;
	TAILQ_ENTRY(tower) towers;
};

struct bullet {
	double x;
	double y;
	int damage;
	struct enemy *target;
	TAILQ_ENTRY(bullet) bullets;
};

#endif 
