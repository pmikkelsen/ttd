#ifndef RENDERING_H
#define RENDERING_H

#include "ttd.h"
#include "gamelogic.h"

void init_graphics();

void render();

#endif
