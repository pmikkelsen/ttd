#version 330 core

in vec2 tex_coord;
out vec4 color;

uniform sampler2D tex;
uniform int is_text;
uniform float color_r;
uniform float color_g;
uniform float color_b;
uniform float color_a;

void main()
{
	vec4 sampled;

	if (is_text == 1) {
		sampled = vec4(1.0, 1.0, 1.0, texture(tex, tex_coord).r);
	} else {
		sampled = texture(tex, tex_coord);
	}

	color = vec4(color_r, color_g, color_b, color_a) * sampled;
}
