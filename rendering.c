#include <GL/glew.h>
#include <sys/queue.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "ttd.h"
#include "utils.h"
#include "gamelogic.h"
#include "rendering.h"

// Use stb_image for loading image data. Initialization is done in stb_image.c
#include "stb_image.h"


// All the different rendering targets.
enum render_target {
	PATH,
	ENEMY,
	TOWER,
	NEW_TOWER,
	NEW_TOWER_RANGE,
	BULLET,
	TEXT_STARTSCREEN,
	TEXT_STATUS,
	TEXT_PAUSE_MENU,
	TEXT_ENEMY_HP,
	BACKGROUND_STARTSCREEN,
	BACKGROUND_GAME,
	BACKGROUND_PAUSE_MENU,
	MAX_RENDER
};

// A RGBA value
struct rgba {
	float r;
	float g;
	float b;
	float a;
};

struct render_data {
	unsigned int vao;
	unsigned int vbo;
	unsigned int texture;

	int is_text;
	float x_pos;
	float y_pos;
	float scale;
	struct rgba rgba;
};

struct render_spec {
	enum render_target target;
	const char *texture_path;
	int is_text;
	float x_pos;
	float y_pos;
	float scale;
	struct rgba rgba;
	int in_states;
};

// Struct containing info about characters to be rendered
struct character {
	unsigned int texture_id;
	int width;
	int height;
	int bearing_x;
	int bearing_y;
	int advance;
} characters[128];

static void draw_target(enum render_target);
static unsigned int compile_shader(const char *, int);
static unsigned int compile_shader_program();
static void set_uniform_float(enum render_target, const char *, float);
static void set_uniform_int(enum render_target, const char *, int);

static void init_font(const char *);
static void render_text(enum render_target, float, float, const char *, ...);
static void render_text_centered(enum render_target, float, const char *, ...);

static const struct render_spec render_specs[MAX_RENDER] = {
	{ ENEMY,
	    .texture_path = "images/ghost.png",
	    .rgba = {1, 1, 1, 1},
	    .scale = 0.9,
	    .in_states = GAME_PAUSED | GAME_PLAYING | GAME_BUY_TOWER
	},
	{ PATH,
	    .texture_path = "images/path.png",
	    .rgba = {1, 1, 1, 1},
	    .scale = 1,
	    .in_states = GAME_PAUSED | GAME_PLAYING | GAME_BUY_TOWER
	},
	{ TOWER,
	    .rgba = {1, 0, 0, 1},
	    .scale = 0.8,
	    .in_states = GAME_PAUSED | GAME_PLAYING | GAME_BUY_TOWER
	},
	{ NEW_TOWER,
	    .rgba = {1, 1, 0, 1},
	    .scale = 0.8,
	    .in_states = GAME_BUY_TOWER
	},
	{ NEW_TOWER_RANGE,
	    .rgba = {1, 0, 0, 0.15},
	    .in_states = GAME_BUY_TOWER
	},
	{ BULLET,
	    .texture_path = "images/bullet.png",
	    .rgba = {1, 1, 1, 0.9},
	    .scale = 0.7,
	    .in_states = GAME_PAUSED | GAME_PLAYING | GAME_BUY_TOWER
	},
	{ TEXT_STARTSCREEN,
	    .is_text = 1,
	    .rgba = {1, 1, 1, 1},
	    .scale = 0.003,
	    .in_states = GAME_STARTSCREEN
	},
	{ TEXT_STATUS,
	    .is_text = 1,
	    .rgba = {1, 1, 1, 1},
	    .scale = 0.0015,
	    .in_states = GAME_PLAYING | GAME_PAUSED | GAME_BUY_TOWER
	},
	{ TEXT_PAUSE_MENU,
	    .is_text = 1,
	    .rgba = {1, 1, 1, 1},
	    .scale = 0.002,
	    .in_states = GAME_PAUSED
	},
	{ TEXT_ENEMY_HP,
	    .is_text = 1,
	    .rgba = {0, 0, 0, 1},
	    .scale = 0.001,
	    .in_states = GAME_PLAYING | GAME_PAUSED | GAME_BUY_TOWER
	},
	{ BACKGROUND_STARTSCREEN,
	    .texture_path = "images/background.png",
	    .rgba = {1, 1, 1, 1},
	    .scale = GRID_SIZE,
	    .in_states = GAME_STARTSCREEN
	},
	{ BACKGROUND_GAME,
	    .texture_path = "images/background.png",
	    .rgba = {1, 1, 1, 1},
	    .scale = GRID_SIZE,
	    .in_states = GAME_PLAYING | GAME_PAUSED | GAME_BUY_TOWER
	},
	{ BACKGROUND_PAUSE_MENU,
	    .texture_path = "images/background.png",
	    .rgba = {1, 1, 1, 1},
	    .scale = GRID_SIZE,
	    .in_states = GAME_PAUSED
	}
};

static struct render_data render_data[MAX_RENDER];
unsigned int shader_program;

void
render()
{
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	glViewport(0, 0, 1000, 800);
	draw_target(BACKGROUND_STARTSCREEN);

	glViewport(100, 0, 800, 800);
	render_text_centered(TEXT_STARTSCREEN, 0, "Press ENTER to start!");

	glViewport(0, 0, 800, 800);
	draw_target(BACKGROUND_GAME);

	// draw the path
	struct coord *c;
	TAILQ_FOREACH(c, &gamestate->path, coords) {
		render_data[PATH].x_pos = grid_pos_to_ndc(c->x);
		render_data[PATH].y_pos = grid_pos_to_ndc(c->y);
		draw_target(PATH);
	}

	// draw the towers
	struct tower *t;
	TAILQ_FOREACH(t, &gamestate->towers, towers) {
		render_data[TOWER].x_pos = grid_pos_to_ndc(t->x);
		render_data[TOWER].y_pos = grid_pos_to_ndc(t->y);
		draw_target(TOWER);
	}

	// draw the bullets from the towers
	struct bullet *b;
	TAILQ_FOREACH(b, &gamestate->bullets, bullets) {
		if (b->target != NULL) {
			render_data[BULLET].x_pos = grid_pos_to_ndc(b->x);
			render_data[BULLET].y_pos = grid_pos_to_ndc(b->y);
			draw_target(BULLET);
		}
	}

	// draw the enemies
	struct enemy *e;
	TAILQ_FOREACH(e, &gamestate->enemies, enemies) {
		render_data[ENEMY].x_pos = grid_pos_to_ndc(e->x);
		render_data[ENEMY].y_pos = grid_pos_to_ndc(e->y);
		draw_target(ENEMY);
	}

	// draw the hp of the enemies
	TAILQ_FOREACH(e, &gamestate->enemies, enemies) {
		render_text(TEXT_ENEMY_HP, grid_pos_to_ndc(e->x)-0.037,
		    grid_pos_to_ndc(e->y)-0.03, "%.0f", e->hp);
	}

	// draw the status text
	glViewport(200, 0, 800, 800);
	render_text(TEXT_STATUS, 0.55, 0.925, "Level: %d", gamestate->level);
	render_text(TEXT_STATUS, 0.55, 0.85, "Lives: %d", gamestate->lives);
	render_text(TEXT_STATUS, 0.55, 0.775, "Money: %d", gamestate->money);

	// draw the new range of the new tower
	glViewport(0, 0, 800, 800);
	if (gamestate->new_tower != NULL) {
		render_data[NEW_TOWER_RANGE].x_pos = grid_pos_to_ndc(gamestate->new_tower->x);
		render_data[NEW_TOWER_RANGE].y_pos = grid_pos_to_ndc(gamestate->new_tower->y);
		render_data[NEW_TOWER_RANGE].scale = gamestate->new_tower->range * 2 + 1;
		draw_target(NEW_TOWER_RANGE);
	}

	// draw the new tower
	glViewport(0, 0, 800, 800);
	if (gamestate->new_tower != NULL) {
		render_data[NEW_TOWER].x_pos = grid_pos_to_ndc(gamestate->new_tower->x);
		render_data[NEW_TOWER].y_pos = grid_pos_to_ndc(gamestate->new_tower->y);
		draw_target(NEW_TOWER);
	}

	glViewport(225, 125, 550, 550);
	draw_target(BACKGROUND_PAUSE_MENU);

	render_text_centered(TEXT_PAUSE_MENU, 0.85, "Game paused");
	render_text_centered(TEXT_PAUSE_MENU, 0.7, "resume the game: SPACE");
	render_text_centered(TEXT_PAUSE_MENU, 0.6, "buy tower: B");
	render_text_centered(TEXT_PAUSE_MENU, 0.5, "cancel purchase: ESCAPE");
	render_text_centered(TEXT_PAUSE_MENU, 0.4, "place tower: ENTER");

}

void
init_graphics()
{
	init_font("/usr/local/share/fonts/inconsolata/Inconsolata.otf");

	struct render_spec spec;

	shader_program = compile_shader_program();
	glUseProgram(shader_program);

	int i;
	for (i = 0; i < MAX_RENDER; i++) {
		spec = render_specs[i];
		glGenBuffers(1, &render_data[spec.target].vbo);
		glGenVertexArrays(1, &render_data[spec.target].vao);

		render_data[spec.target].is_text = spec.is_text;
		render_data[spec.target].rgba	 = spec.rgba;
		render_data[spec.target].x_pos	 = spec.x_pos;
		render_data[spec.target].y_pos	 = spec.y_pos;
		render_data[spec.target].scale	 = spec.scale;

		// if specified, load the texture, otherwise fill the buffer with white
		int w = 1;
		int h = 1;
		int channels = 4;
		unsigned char white_data[] = { 255, 255, 255, 255 };
		unsigned char *tex_data = white_data;

		if (spec.texture_path) {
			tex_data = stbi_load(spec.texture_path, &w, &h, &channels, 0);

			if (channels != 4) {
				printf("Image: %s, not in RGBA format!\n",
				    spec.texture_path);	
				exit(EXIT_FAILURE);
			}
		}
		glGenTextures(1, &render_data[spec.target].texture);
		glBindTexture(GL_TEXTURE_2D, render_data[spec.target].texture);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA,
		    GL_UNSIGNED_BYTE, tex_data);
		glGenerateMipmap(GL_TEXTURE_2D);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

		if (tex_data != white_data)
			stbi_image_free(tex_data);

		// Bind the VAO and VBO before and setup the vertex data
		glBindVertexArray(render_data[spec.target].vao);
		glBindBuffer(GL_ARRAY_BUFFER, render_data[spec.target].vbo);

		// a square with the size of one grid cell
		// format: x y - texture x texture y
		float offset = 2.0/GRID_SIZE/2.0;
		float vertices[6][4] = {
		    { -offset,  offset, 0.0, 0.0 },
		    { -offset, -offset, 0.0, 1.0 },
		    {  offset, -offset, 1.0, 1.0 },
		    { -offset,  offset, 0.0, 0.0 },
		    {  offset, -offset, 1.0, 1.0 },
		    {  offset,  offset, 1.0, 0.0 },
		};

		// If it is text, we use DYNAMIC_DRAW, otherwise just STATIC_DRAW
		if (spec.is_text) {
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
			    &vertices, GL_DYNAMIC_DRAW);
		} else {
			glBufferData(GL_ARRAY_BUFFER, sizeof(vertices),
			    &vertices, GL_STATIC_DRAW);
		}

		glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
		glEnableVertexAttribArray(0);
	}
}

///
// SHADER FUNCTIONS
///

static void
set_uniform_float(enum render_target target, const char *location_name, float value)
{
	int uniform_location = glGetUniformLocation(
	    shader_program, location_name);

	if (uniform_location == -1) {
		printf("set_uniform_float: %s is not an active uniform variable\n",
		    location_name);
		exit(EXIT_FAILURE);
	}
	glUniform1f(uniform_location, value);
}

static void
set_uniform_int(enum render_target target, const char *location_name, int value)
{
	int uniform_location = glGetUniformLocation(
	    shader_program, location_name);

	if (uniform_location == -1) {
		printf("set_uniform_int: %s is not an active uniform variable\n",
		    location_name);
		exit(EXIT_FAILURE);
	}
	glUniform1i(uniform_location, value);
}

static unsigned int
compile_shader(const char *shader_path, int shader_type)
{
	// Random guess at how large a shader is max.
	const int MAX_SHADER_SOURCE_LENGTH = 16384;

	char *shader_source = malloc(MAX_SHADER_SOURCE_LENGTH);
	if (shader_source == NULL)
		printf("compile_shader: could not malloc\n");

	FILE *fp = fopen(shader_path, "r");
	if (fp == NULL)
		printf("compile_shader: could not open file: %s\n", shader_path);
	fread(shader_source, sizeof(char), MAX_SHADER_SOURCE_LENGTH, fp);
	fclose(fp);
	const char *source = shader_source;

	unsigned int shader = glCreateShader(shader_type);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);

	int success;
	char info_log[512];
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(shader, 512, NULL, info_log);
		printf("could not compile shader (%s):\n%s\n", shader_path, info_log);
	}

	free(shader_source);
	return shader;
}

static unsigned int
compile_shader_program()
{
	unsigned int program = glCreateProgram();

	unsigned int vertex_shader = compile_shader("shaders/vertex.glsl",
	    GL_VERTEX_SHADER);
	glAttachShader(program, vertex_shader);

	unsigned int fragment_shader = compile_shader("shaders/fragment.glsl",
	    GL_FRAGMENT_SHADER);
	glAttachShader(program, fragment_shader);

	glLinkProgram(program);

	int success;
	char info_log[512];
	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, 512, NULL, info_log);
		printf("shader program linking failed:\n%s\n", info_log);
	}

	glDeleteShader(vertex_shader);
	glDeleteProgram(fragment_shader);

	return program;
}

///
// Text functions
///

static void
init_font(const char *font_path)
{
	FT_Library ft;
	FT_Face face;

	if (FT_Init_FreeType(&ft) != 0)
		printf("FreeType init failed\n");

	if (FT_New_Face(ft, font_path, 0, &face) != 0)
		printf("FreeType new face failed\n");

	if (FT_Set_Pixel_Sizes(face, 0, 48) != 0)
		printf("FreeType set pixel size failed\n");

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	int c;
	for (c = 0; c < 128; c++) {
		if (FT_Load_Char(face, c, FT_LOAD_RENDER) != 0) {
			printf("Failed to load glyph %d\n", c);
			continue;
		}

		unsigned int texture;
		glGenTextures(1, &texture);
		glBindTexture(GL_TEXTURE_2D, texture);
		glTexImage2D(GL_TEXTURE_2D,
		    0,
		    GL_RED,
		    face->glyph->bitmap.width,
		    face->glyph->bitmap.rows,
		    0,
		    GL_RED,
		    GL_UNSIGNED_BYTE,
		    face->glyph->bitmap.buffer
		);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		characters[c].texture_id = texture;
		characters[c].width = face->glyph->bitmap.width;
		characters[c].height = face->glyph->bitmap.rows;
		characters[c].bearing_x = face->glyph->bitmap_left;
		characters[c].bearing_y = face->glyph->bitmap_top;
		characters[c].advance = face->glyph->advance.x;
	}

	FT_Done_Face(face);
	FT_Done_FreeType(ft);
}

static void
render_text(enum render_target target, float x, float y, const char *format, ...)
{
	if ((render_specs[target].in_states & gamestate->game_status) == 0)
		return;

	va_list ap;
	va_start(ap, format);

	char *string = malloc(512);
	memset(string, 0, 512);
	vsnprintf(string, 512, format, ap);
	va_end(ap);

	float scale = render_data[target].scale;

	int c;
	for (c = 0; string[c] != '\0'; c++) {
		struct character ch = characters[(int)string[c]];

		float xpos = x + ch.bearing_x * scale;
		float ypos = y - (ch.height - ch.bearing_y) * scale;

		float w = ch.width * scale;
		float h = ch.height * scale;

		float vertices[6][4] = {
		    { 0,	0 + h,		0.0,	0.0	},
		    { 0,	0,		0.0,	1.0	},
		    { 0 + w,	0,		1.0,	1.0	},

		    { 0,	0 + h,		0.0,	0.0	},
		    { 0 + w,	0,		1.0,	1.0	},
		    { 0 + w,	0 + h,		1.0,	0.0	}
		};

		set_uniform_int(target, "is_text", render_data[target].is_text);
		set_uniform_float(target, "color_r", render_data[target].rgba.r);
		set_uniform_float(target, "color_g", render_data[target].rgba.g);
		set_uniform_float(target, "color_b", render_data[target].rgba.b);
		set_uniform_float(target, "color_a", render_data[target].rgba.a);
		set_uniform_float(target, "x_pos", xpos);
		set_uniform_float(target, "y_pos", ypos);
		set_uniform_float(target, "scale", 1);

		glBindTexture(GL_TEXTURE_2D, ch.texture_id);
		glBindVertexArray(render_data[target].vao);
		glBindBuffer(GL_ARRAY_BUFFER, render_data[target].vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
		glDrawArrays(GL_TRIANGLES, 0, 6);

		x += (ch.advance >> 6) * scale;
	}
	free(string);
}

static void
render_text_centered(enum render_target target, float y, const char *format, ...)
{
	if ((render_specs[target].in_states & gamestate->game_status) == 0)
		return;

	va_list ap;
	va_start(ap, format);

	char *string = malloc(512);
	memset(string, 0, 512);
	vsnprintf(string, 512, format, ap);

	float string_width = 0;

	int c;
	for (c = 0; string[c] != '\0'; c++) {
		struct character ch = characters[(int)string[c]];
		string_width += (ch.advance >> 6) * render_data[target].scale;
	}

	render_text(target, 0 - string_width/2, y, format, ap);
	va_end(ap);
	free(string);
}

///
// Drawing functions
///

static void
draw_target(enum render_target target)
{
	if ((render_specs[target].in_states & gamestate->game_status) == 0)
		return;

	glBindTexture(GL_TEXTURE_2D, render_data[target].texture);
	glBindVertexArray(render_data[target].vao);
	set_uniform_int(target, "is_text", render_data[target].is_text);
	set_uniform_float(target, "color_r", render_data[target].rgba.r);
	set_uniform_float(target, "color_g", render_data[target].rgba.g);
	set_uniform_float(target, "color_b", render_data[target].rgba.b);
	set_uniform_float(target, "color_a", render_data[target].rgba.a);
	set_uniform_float(target, "x_pos", render_data[target].x_pos);
	set_uniform_float(target, "y_pos", render_data[target].y_pos);
	set_uniform_float(target, "scale", render_data[target].scale);
	glDrawArrays(GL_TRIANGLES, 0, 6);
}
