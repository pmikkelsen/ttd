#include <math.h>

#include "ttd.h"
#include "utils.h"

double
grid_pos_to_ndc(double x)
{
	return (x - (GRID_SIZE-1)/2) * (2.0/GRID_SIZE);
}

int
in_range(struct tower *tower, struct enemy *enemy)
{
	double delta_x = fabs(tower->x - enemy->x) - 0.5;
	double delta_y = fabs(tower->y - enemy->y) - 0.5;

	if (delta_x <= tower->range && delta_y <= tower->range)
		return 1;
	else
		return 0;
}
