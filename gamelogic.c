#include <sys/queue.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "ttd.h"
#include "utils.h"
#include "gamelogic.h"

struct gamestate *gamestate;

int
get_game_status()
{
	return gamestate->game_status;
}

void
set_game_status(enum game_status status)
{
	gamestate->game_status = status;
}

void
buy_tower()
{
	// FIXME the price and recharge time
	if (gamestate->game_status != GAME_BUY_TOWER && gamestate->money >= 1) {
		set_game_status(GAME_BUY_TOWER);
		gamestate->new_tower = malloc(sizeof(struct tower));
		gamestate->new_tower->x = 1;
		gamestate->new_tower->y = 1;
		gamestate->new_tower->range = 1;
		gamestate->new_tower->damage = 50;
		gamestate->new_tower->recharge_time = 1;
		gamestate->new_tower->time_to_ready = 0;
	}
}

void
move_new_tower(int x_delta, int y_delta)
{
	int new_x = gamestate->new_tower->x + x_delta;
	int new_y = gamestate->new_tower->y + y_delta;

	if (new_x < GRID_SIZE && new_x >= 0)
		gamestate->new_tower->x = new_x;

	if (new_y < GRID_SIZE && new_y >= 0)
		gamestate->new_tower->y = new_y;
}

void
place_tower()
{
	int on_path = 0;
	int on_another_tower = 0;

	struct coord *c;
	TAILQ_FOREACH(c, &gamestate->path, coords) {
		if (c->x == gamestate->new_tower->x &&
		    c->y == gamestate->new_tower->y)
			on_path = 1;
	}

	struct tower *t;
	TAILQ_FOREACH(t, &gamestate->towers, towers) {
		if (t->x == gamestate->new_tower->x &&
		    t->y == gamestate->new_tower->y)
			on_another_tower = 1;
	}

	if (!on_path && !on_another_tower &&
	    gamestate->game_status == GAME_BUY_TOWER &&
	    gamestate->new_tower != NULL) {
		gamestate->money -= 1;
		TAILQ_INSERT_HEAD(&gamestate->towers, gamestate->new_tower, towers);
		gamestate->new_tower = NULL;
		set_game_status(GAME_PLAYING);
	}
}

static void
load_path(const char *path_file)
{
	FILE *fp = fopen(path_file, "r");
	double x, y;
	struct coord *c;

	fscanf(fp, "# Path");

	while (fscanf(fp, "%lf %lf", &x, &y) == 2) {
		c = malloc(sizeof(struct coord));
		c->x = x;
		c->y = y;
		c->dir_to_next = END;
		TAILQ_INSERT_TAIL(&gamestate->path, c, coords);
	}

	gamestate->path_length = 0;
	TAILQ_FOREACH(c, &gamestate->path, coords) {
		gamestate->path_length++;

		if (TAILQ_NEXT(c, coords) == NULL) {
			c->dir_to_next = END;
		} else if (c->x < TAILQ_NEXT(c, coords)->x) {
			c->dir_to_next = EAST;
		} else if (c->x > TAILQ_NEXT(c, coords)->x) {
			c->dir_to_next = WEST;
		} else if (c->y < TAILQ_NEXT(c, coords)->y) {
			c->dir_to_next = NORTH;
		} else if (c->y > TAILQ_NEXT(c, coords)->y) {
			c->dir_to_next = SOUTH;
		}
	}

	fclose(fp);

}

static void
load_enemy_level(const char *enemy_file)
{
	FILE *fp = fopen(enemy_file, "r");

	char tmp[100];

	int level = 0;
	while (level != gamestate->level) {
		int ret = fscanf(fp, "# Level %d", &level);
		if (ret == EOF) {
			printf("No more enemy levels!\n");
			exit(EXIT_FAILURE);
		} else if (ret != 1) {
			fgets(tmp, 100, fp);
		}
	}

	int speed, hp;
	double delay;
	struct enemy *e;
	while (fscanf(fp, "%d %d %lf",  &speed, &hp, &delay) == 3) {
		e = malloc(sizeof(struct enemy));
		e->path = &gamestate->path;
		e->current = TAILQ_FIRST(&gamestate->path);
		e->x = e->current->x;
		e->y = e->current->y;
		e->speed = speed;
		e->distance_traveled = 0;
		e->hp = hp;
		e->money_drop = hp/100; 
		e->start_delay = delay;
		TAILQ_INSERT_HEAD(&gamestate->waiting_enemies, e, enemies);
	}

	fclose(fp);

}

struct gamestate *
init_game(const char *path_file)
{
	gamestate = malloc(sizeof(struct gamestate));

	gamestate->game_status = GAME_STARTSCREEN;
	gamestate->lives = 1;
	gamestate->money = 1;
	gamestate->level = 0;

	TAILQ_INIT(&gamestate->path);
	TAILQ_INIT(&gamestate->enemies);
	TAILQ_INIT(&gamestate->waiting_enemies);
	TAILQ_INIT(&gamestate->towers);
	TAILQ_INIT(&gamestate->bullets);
	load_path(path_file);

	gamestate->new_tower = NULL;

	return gamestate;
}

void
update_gamestate(const char *enemy_file, double time_delta)
{

if (gamestate->game_status == GAME_PLAYING ||
    gamestate->game_status == GAME_BUY_TOWER) {
	struct enemy *e, *e_tmp;
	struct tower *t;
	struct bullet *b, *b_tmp;

	// for each waiting enemy, check if it is ready to be in the game
	TAILQ_FOREACH_SAFE(e, &gamestate->waiting_enemies, enemies, e_tmp) {
		if (e->start_delay > 0) {
			e->start_delay -= time_delta;
		} else {
			TAILQ_REMOVE(&gamestate->waiting_enemies, e, enemies);
			TAILQ_INSERT_HEAD(&gamestate->enemies, e, enemies);
		}
	}

	// for each tower, select the front-most enemy in range and
	// shoot a bullet after it if the tower is ready
	TAILQ_FOREACH(t, &gamestate->towers, towers) {
		if (t->time_to_ready > 0) {
			t->time_to_ready -= time_delta;
			continue;
		}

		struct enemy *target = NULL;
		TAILQ_FOREACH(e, &gamestate->enemies, enemies) {
			if (in_range(t, e)) {
				if (target == NULL)
					target = e;
				if (e->distance_traveled > target->distance_traveled)
					target = e;
			}
		}

		// if there was an enemy in the range, shoot a bullet
		if (target != NULL) {
			t->time_to_ready = t->recharge_time;
			struct bullet *new_bullet = malloc(sizeof(struct bullet));
			new_bullet->x = t->x;
			new_bullet->y = t->y;
			new_bullet->damage = t->damage;
			new_bullet->target = target;
			TAILQ_INSERT_HEAD(&gamestate->bullets, new_bullet, bullets);
		} else {
			t->time_to_ready = 0;
		}
	}

	// Move all bullets, and apply damage if the bullet hits
	TAILQ_FOREACH_SAFE(b, &gamestate->bullets, bullets, b_tmp) {
		double x_distance = b->target->x - b->x;
		double y_distance = b->target->y - b->y;
		double distance = sqrt(x_distance * x_distance + y_distance * y_distance);
		double x_factor = x_distance / distance;
		double y_factor = y_distance / distance;

		b->x += 3 * time_delta * x_factor;
		b->y += 3 * time_delta * y_factor;

		// If the bullet has hit (or close enough), apply damage
		// and remove the bullet
		if (fabs(b->x - b->target->x) <= 0.5 &&
		    fabs(b->y - b->target->y) <= 0.5) {
			b->target->hp -= b->damage;
			TAILQ_REMOVE(&gamestate->bullets, b, bullets);
			free(b);
		}
	}

	// remove all enemies that are dead, and add the correct amount of money
	// and remove all bullets going after them
	TAILQ_FOREACH_SAFE(e, &gamestate->enemies, enemies, e_tmp) {
		if (e->hp <= 0) {
			gamestate->money += e->money_drop;

			TAILQ_FOREACH_SAFE(b, &gamestate->bullets, bullets, b_tmp) {
				if (b->target == e) {
					TAILQ_REMOVE(&gamestate->bullets, b, bullets);
					free(b);
				}
			}
			TAILQ_REMOVE(&gamestate->enemies, e, enemies);
			free(e);
		}
	}

	// move all enemies, and remove those that are out of the track
	TAILQ_FOREACH_SAFE(e, &gamestate->enemies, enemies, e_tmp) {
		int direction = e->current->dir_to_next;
		switch (direction) {
		case SOUTH:
			e->y -= e->speed * time_delta;
			break;
		case EAST:
			e->x += e->speed * time_delta;
			break;
		case NORTH:
			e->y += e->speed * time_delta;
			break;
		case WEST:
			e->x -= e->speed * time_delta;
			break;
		}

		e->distance_traveled += e->speed * time_delta;

		double delta_x = e->x - TAILQ_NEXT(e->current, coords)->x;
		double delta_y = e->y - TAILQ_NEXT(e->current, coords)->y;

		if ((direction == SOUTH && delta_y <= 0) ||
		    (direction == EAST  && delta_x >= 0) ||
		    (direction == NORTH && delta_y >= 0) ||
		    (direction == WEST  && delta_x <= 0)) {
			e->current = TAILQ_NEXT(e->current, coords);
			e->y = e->current->y;
			e->x = e->current->x;
		}

		// If the next tile is the end, the enemy made it all the way.
		if (e->current->dir_to_next == END) {
			TAILQ_REMOVE(&gamestate->enemies, e, enemies);
			free(e);
			gamestate->lives--;
		}
	}

	// If we have no lives left, the game is over.
	if (gamestate->lives <= 0) {
		set_game_status(GAME_GAMEOVER);
	}

	// If there is no enemies left, load the next level
	if (TAILQ_EMPTY(&gamestate->enemies) && TAILQ_EMPTY(&gamestate->waiting_enemies)) {
		gamestate->level++;
		load_enemy_level(enemy_file);
	}
}
}
