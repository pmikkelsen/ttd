#include <GLFW/glfw3.h>
#include <stdio.h>

#include "ttd.h"
#include "gamelogic.h"
#include "input.h"


void key_callback(GLFWwindow* win, int key, int scancode, int action, int mods)
{

#define IF_KEY_PRESSED(k) if (key == GLFW_KEY_ ## k && (action == GLFW_PRESS || action == GLFW_REPEAT))
#define CHANGE_GAME_STATUS(from, to) \
    if (game_status == GAME_ ## from) \
	set_game_status(GAME_ ## to)

	int game_status = get_game_status();

	IF_KEY_PRESSED(ENTER) {
		CHANGE_GAME_STATUS(STARTSCREEN, PLAYING);
	}

	IF_KEY_PRESSED(SPACE) {
		CHANGE_GAME_STATUS(PLAYING, PAUSED);
		CHANGE_GAME_STATUS(PAUSED, PLAYING);
	}

	IF_KEY_PRESSED(ESCAPE) {
		CHANGE_GAME_STATUS(BUY_TOWER, PLAYING);
	}

	IF_KEY_PRESSED(B) {
		if (game_status == GAME_PLAYING)
			buy_tower();
	}

	if (game_status == GAME_BUY_TOWER) {
		IF_KEY_PRESSED(UP)
			move_new_tower(0,1);
		IF_KEY_PRESSED(DOWN)
			move_new_tower(0,-1);
		IF_KEY_PRESSED(LEFT)
			move_new_tower(-1,0);
		IF_KEY_PRESSED(RIGHT)
			move_new_tower(1,0);
		IF_KEY_PRESSED(ENTER)
			place_tower();
	}
}
