#version 330 core
layout (location = 0) in vec4 vertex;

out vec2 tex_coord;

uniform float x_pos;
uniform float y_pos;
uniform float scale;

void main()
{
	vec2 pos = vertex.xy * scale;
	gl_Position = vec4(pos.x + x_pos, pos.y + y_pos, 0, 1.0);
	tex_coord = vertex.zw;
}
