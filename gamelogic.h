#ifndef GAMELOGIC_H
#define GAMELOGIC_H

#include <sys/queue.h>

enum game_status {
	GAME_STARTSCREEN = 0x01,
	GAME_PLAYING 	 = 0x02,
	GAME_PAUSED	 = 0x04,
	GAME_GAMEOVER	 = 0x08,
	GAME_BUY_TOWER	 = 0x10
};

struct gamestate {
	int game_status;

	int lives;
	int money;
	int level;

	struct enemy_list enemies;
	struct enemy_list waiting_enemies;
	struct path_list path;
	struct tower_list towers;
	struct bullet_list bullets;

	struct tower *new_tower;

	int path_length;
};

extern struct gamestate *gamestate;

struct gamestate *init_game(const char *);

void update_gamestate(const char *, double);

int get_game_status();

void set_game_status();

void buy_tower();

void move_new_tower(int, int);

void place_tower();

#endif
