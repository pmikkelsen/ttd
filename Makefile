PROG = ttd

SRCS += ttd.c
SRCS += utils.c
SRCS += gamelogic.c
SRCS += rendering.c
SRCS += input.c
SRCS += stb_image.c

NOMAN = noman

COPTS += -Wall -pthread -std=c11
COPTS += `pkg-config --cflags-only-I glfw3` 
COPTS += `pkg-config --cflags-only-I glew` 
COPTS += `pkg-config --cflags-only-I gl`
COPTS += `pkg-config --cflags-only-I freetype2`

LDFLAGS += `pkg-config --static --libs-only-L glfw3`
LDFLAGS += `pkg-config --static --libs-only-L glew`
LDFLAGS += `pkg-config --static --libs-only-L gl`
LDFLAGS += `pkg-config --static --libs-only-L freetype2`

LDADD += `pkg-config --static --libs-only-l glfw3` 
LDADD += `pkg-config --static --libs-only-l glew` 
LDADD += `pkg-config --static --libs-only-l gl` 
LDADD += `pkg-config --static --libs-only-l freetype2`

.include <bsd.prog.mk>
